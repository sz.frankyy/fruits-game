let y = 0;
let player;
let players = [];
let playerFrames
let loop = 30;
let playerSh;
let fruit;
let fruits = [];
const FRUITS_HEIGHT = 8;
const FRUITS_LENGHT = 7;
const PLAYER_FRAMES_LENGTH = 29;
const WIDTH = 800;
const HEIGHT = 600;
let keyboardOn = false;
let playerPosition = 0;
let playerCurrentFrame = 0;
let camera;
let points = 0;
let animCounter = 0;
class Example extends Phaser.Scene {
    constructor() {
        super();
    }

    preload() {
        for (let i = 1; i <= FRUITS_HEIGHT; i++) {
            for (let j = 1; j <= FRUITS_LENGHT; j++) {
                fruits.push(this.load.image(`fruit-${j}-${i}`, `img/row-${j}-col-${i}.jpg`));
            }
        }


        for (let i = 1; i <= PLAYER_FRAMES_LENGTH; i++) {

            players.push(this.load.image(`player-${i}`, 'img/playerAnim/frame_' + (i < 10 ? '0' + i : i) + '_delay-0.1s.gif'));

        }

        // this.load.image('player', 'img/player.gif');
        this.anims.create({
            key: 'walk',
            frames: [
                { key: 'char', frame: 1 },
                { key: 'char', frame: 2 },
            ],
            frameRate: 8,
            repeat: -1
        });
    }

    create() {
        const that = this;

        this.points = this.add.text(100, 100, points, {
            font: "65px Arial",
            fill: "#ff0044",
            align: "center"
        });
        player = this.physics.add.sprite();
        
        player.x = (WIDTH-player.width)/2

        fruit = this.physics.add.sprite(32, 0);

        createNewFruit(this)
        player.body.allowGravity = false


        this.input.keyboard.on('keydown-A', function (event) {
            player.x -= 10;
            playerCurrentFrame+=1;

            playerCurrentFrame = Math.min(Math.min(playerCurrentFrame, 24), 29);
            debugger
      
            player.setTexture(`player-${playerCurrentFrame}`)
            playerPosition = -1;
            keyboardOn = keyboardOn || true;
            // console.log(keyboardOn)

        });
        this.input.keyboard.on('keydown-D', function (event) {
            playerCurrentFrame+=1;
            player.x -= -10;
            debugger
            // if(playerCurrentFrame>=22) {
                
            // }
            // if(playerCurrentFrame>=18) {
            //     playerCurrentFrame+=1;
            // }
            // playerCurrentFrame = Math.min(Math.min(playerCurrentFrame + 1, 18), 23);
            // console.log(`player-${playerCurrentFrame}`)
            player.setTexture(`player-${playerCurrentFrame}`)

            playerPosition = 1;
            keyboardOn = keyboardOn || true;
            // console.log(keyboardOn)

        });

        this.input.keyboard.on('key-A',()=>{
            keyboardOn = false;
        })
        this.input.keyboard.on('key-D',()=>{
            keyboardOn = false;
        })
        this.physics.add.collider(player, fruit, (e) => {
              points+=10;
             createNewFruit(this)
        }, null, this)





    }

    update() {
        if (animCounter < 20) {
            animCounter += 1;
        } else {
            animCounter = 0;
        }


        if (animCounter === 19 && !keyboardOn) {
            //console.log('!keyboardOn')

            playerCurrentFrame = (playerCurrentFrame < 10 ? playerCurrentFrame += 1 : 0)
            keyboardOn  || player.setTexture(`player-${playerCurrentFrame}`);
        }

        if(keyboardOn) {
            
            switch (playerPosition) {
                
                case 1: playerCurrentFrame = (playerCurrentFrame < 19 ? playerCurrentFrame : 21); break;
                // case 0: playerCurrentFrame = (playerCurrentFrame < 10 ? playerCurrentFrame += 1 : 0); break;
                case -1: playerCurrentFrame = (playerCurrentFrame < 29 ? playerCurrentFrame += 1 : 24); break;
            }
        }
        
        player.y = 450;

        if (fruit.y >= 600) {
            loop-=1;
            createNewFruit(this);
        }


    }
}

function createNewFruit(that) {

    fruit.y = 0;

    fruit.x = Math.floor(Math.random() * 800)
    let height = Math.max(Math.floor(Math.random() * (FRUITS_HEIGHT + 1)) - 1, 1);
    let length = Math.max(Math.floor(Math.random() * (FRUITS_LENGHT + 1)) - 1, 1);


    fruit.setTexture(`fruit-${height}-${length}`);
    fruit.body.speed = 0;
    fruit.body.velocity.y = 0;
    that.points.setText(`Punkty ${points}, życia ${loop}`)
    if (loop === 0) {
        that.scene.pause();
        that.points.setText(`Uzyskałeś ${points} punktów`);

    }

}

const config = {
    type: Phaser.AUTO,
    parent: 'phaser-example',
    width: WIDTH,
    height: HEIGHT,
    scene: [Example],
    physics: {
        default: 'arcade',
        arcade: {
            debug: true,
            gravity: { y: 200 }
        }
    },
    //   plugins: {
    //     global: [ NineSlicePlugin.DefaultCfg ],
    //   },
};

const game = new Phaser.Game(config);
